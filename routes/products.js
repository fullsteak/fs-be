const express = require("express");
const router = express.Router();
const authenticate = require("../middlewares/verifyToken");
const productController = require("../controllers/products.controller");


// all routes in here are starting with '/products'
router.get("/category/:categoryId", productController.findAllProductsByCategory);
router.get("/get", productController.findAllProductsPaginate);  //TODO: The path can be changed
router.get("/:id", productController.getProduct);
router.get("/", productController.findAllProducts);

// Protected routes
router.post("/", authenticate, productController.createProduct);
router.put("/:id", authenticate, productController.updateProduct);
router.delete("/:id", authenticate, productController.deleteProduct);

module.exports = router;

