const express = require("express");
const router = express.Router();
const authenticate = require("../middlewares/verifyToken");
const productLibrariesController = require("../controllers/productLibraries.controller");

// all routes in here are starting with '/productLibraries'

router.get("/", productLibrariesController.findAllProductLibraries);
router.get("/:id", productLibrariesController.getProductLibrary);

// Protected routes
router.post("/", authenticate, productLibrariesController.createProductLibrary);
router.put("/:id", authenticate, productLibrariesController.updateProductLibrary);
router.delete("/:id", authenticate, productLibrariesController.deleteProductLibrary);


module.exports = router;
