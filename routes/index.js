const auth = require("./auth");
const users = require("./users");
const roles = require("./roles");
const userRoles = require("./userRoles");
const categories = require("./categories");
const prices = require("./prices");
const libraries = require("./libraries");
const products = require("./products");
const productCategories = require("./productCategories");
const productLibraries = require("./productLibraries");

const routes = {

    auth,
    users,
    roles,
    userRoles,
    categories,
    prices,
    libraries,
    products,
    productCategories,
    productLibraries
};

module.exports = routes;
