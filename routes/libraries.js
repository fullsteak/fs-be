const express = require("express");
const router = express.Router();
const authenticate = require("../middlewares/verifyToken");
const librariesController = require("../controllers/libraries.controller");

// all routes in here are starting with '/libraries'

router.get("/", librariesController.findAllLibraries);
router.get("/:id", librariesController.getLibrary);

// Protected routes
router.post("/", authenticate, librariesController.createLibrary);
router.put("/:id", authenticate, librariesController.updateLibrary);
router.delete("/:id", authenticate, librariesController.deleteLibrary);

module.exports = router;
