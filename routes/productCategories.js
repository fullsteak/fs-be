const express = require("express");
const router = express.Router();
const authenticate = require("../middlewares/verifyToken");
const productCategoriesController = require("../controllers/productCategories.controller");

// all routes in here are starting with '/productCategories'

router.get("/", productCategoriesController.findAllProductCategories);
router.get("/:id", productCategoriesController.getProductCategory);

// Protected routes
router.post("/", authenticate, productCategoriesController.createProductCategory);
router.put("/:id", authenticate, productCategoriesController.updateProductCategory);
router.delete("/:id", authenticate, productCategoriesController.deleteProductCategory);

module.exports = router;
