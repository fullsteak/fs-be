const express = require("express");
const router = express.Router();
const authenticate = require("../middlewares/verifyToken");
const pricesController = require("../controllers/prices.controller");

// all routes in here are starting with '/prices'

router.get("/", pricesController.findAllPrices);
router.get("/:id", pricesController.getPrice);

// Protected routes
router.post("/", authenticate, pricesController.createPrice);
router.put("/:id", authenticate, pricesController.updatePrice);
router.delete("/:id", authenticate, pricesController.deletePrice);

module.exports = router;