const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');
const product = require("./Product");

const Price = sequelize.define('price', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  title: {
    type: DataTypes.STRING(60),
    allowNull: false,
    // unique: true
  },
  try: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  usd: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  euro: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  salePrice: {
    type: DataTypes.DOUBLE,
    allowNull: true
  },
  productId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: 'Products',
      key: 'id'
    }
  }
}, {
  // Other model options go here
  timestamps: true,
  tableName: 'Prices'
});

product.hasMany(Price);

module.exports = Price;