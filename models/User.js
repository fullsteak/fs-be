const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const User = sequelize.define('user', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  firstName: {
    type: DataTypes.STRING(50),
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING(50),
    allowNull: false
  },
  email: {
    type: DataTypes.STRING(62),
    allowNull: false,
    unique: true
  },
  password: {
    type: DataTypes.STRING(72),
    allowNull: false
  }
}, {
  // Other model options go here
  timestamps: true,
  tableName: 'Users'
});

// `sequelize.define` also returns the model
// console.log(User === sequelize.models.User); // true

// User.sync().then(()=>{
//     console.log("User table and the model synced successfully!" + User === sequelize.models.User); 
// }).catch((err)=>{
//     console.log("Error syncing the User table and model.")
// })

module.exports = User;