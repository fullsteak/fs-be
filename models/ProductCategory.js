const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');
const product = require("./Product");
const category = require("./Category");

const ProductCategory = sequelize.define('productCategory', {
  // Model attributes are defined here
  id: {
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  }
}, {
  // Other model options go here
  timestamps: true,
  tableName: 'ProductCategories',
});

product.belongsToMany(category, { through: ProductCategory, foreignKey:{allowNull:false, name:'productId'}});
category.belongsToMany(product, { through: ProductCategory, foreignKey:{allowNull:false, name:'categoryId'}});
ProductCategory.belongsTo(product);
ProductCategory.belongsTo(category);
product.hasMany(ProductCategory, {foreingKey: { name: "userId", allowNull: false }, onDelete: "CASCADE" });
category.hasMany(ProductCategory, {foreingKey: { name: "roleId", allowNull: false }, onDelete: "CASCADE" });

module.exports = ProductCategory;