const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const Library = sequelize.define('library', {
  // Model attributes are defined here
  id: {
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  path: {
    type: DataTypes.STRING(250),
    allowNull: false,
    unique: true
  },
  type: {
    type: DataTypes.STRING(20),
    allowNull: true
  }
}, {
  // Other model options go here
  timestamps: true,
  tableName: 'Libraries'
});

module.exports = Library;