const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');
const product = require("./Product");
const library = require("./Library");

const ProductLibrary = sequelize.define('productLibrary', {
  // Model attributes are defined here
  id: {
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
}, {
  // Other model options go here
  timestamps: true,
  tableName: 'ProductLibraries',
});

product.belongsToMany(library,{ through: ProductLibrary, foreignKey:{allowNull:false, name:'productId'}});
library.belongsToMany(product, {through: ProductLibrary, foreignKey:{allowNull:false, name:'libraryId'}});
ProductLibrary.belongsTo(product);
ProductLibrary.belongsTo(library);
product.hasMany(ProductLibrary, {foreingKey: { name: "productId", allowNull: false }, onDelete: "CASCADE" });
library.hasMany(ProductLibrary, {foreingKey: { name: "libraryId", allowNull: false }, onDelete: "CASCADE" });


module.exports = ProductLibrary;