const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const Category = sequelize.define('category', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  name: {
    type: DataTypes.STRING(50),
    allowNull: false,
    unique: true
  },
  parentId: {
    type: DataTypes.INTEGER,
    allowNull: true,
    references: {
        model: 'Categories',
        key: 'id'
    }
  }
}, {
  // Other model options go here
  timestamps: true,
  tableName: 'Categories'
});

module.exports = Category;