"use strict";
const { saltAndHashPassword } = require('../utils/password');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        // Add seed commands here.Example:
        console.log("Users")

        await queryInterface.bulkInsert("Users",
            [
                {
                    
                    firstName: "John",
                    lastName: "Doe",
                    email: "john@example.com",
                    password: await saltAndHashPassword("123456"),
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    firstName: "Marry",
                    lastName: "Jane",
                    email: "mary@example.com",
                    password: await saltAndHashPassword("123456"),
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    firstName: "Jane",
                    lastName: "Doe",
                    email: "jane@example.com",
                    password: await saltAndHashPassword("123456"),
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        );
        console.log("Roles")


        await queryInterface.bulkInsert( "Roles",
            [
                {
                    
                    name: "Super Admin",
                    isActive: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    name: "Admin",
                    isActive: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    name: "User",
                    isActive: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    name: "Default",
                    isActive: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                }
            ],
            {}
        );
        console.log("UserRoles")

        await queryInterface.bulkInsert("UserRoles",
            [
                {
                    
                    userId: 1,
                    roleId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    userId: 2,
                    roleId: 2,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        );
        console.log("Categories")

        await queryInterface.bulkInsert("Categories",
            [
                {
                    // 1
                    name: "Electronics",
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 2
                    name: "Smart Home",
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 3
                    name: "Home and Kitchen",
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {  
                    // 4
                    name: "Sports and Outdoors",
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 5
                    name: "Cell Phones & Accessories",
                    parentId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 6
                    name: "Computers & Accessories",
                    parentId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 7
                    name: "Wearable Technology",
                    parentId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 8
                    name: "Smartwatches",
                    parentId: 7,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    //9
                    name: "Rings",
                    parentId: 7,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 10
                    name: "Kitchen and Dining",
                    parentId: 3,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 11
                    name: "Bedding",
                    parentId: 3,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 12
                    name: "Furniture",
                    parentId: 3,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 13
                    name: "Sports & Fitness",
                    parentId: 4,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 14   
                    name: "Outdoor Recreation",
                    parentId: 4,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                }
            ],
            {}
        );
        console.log("Products")

        await queryInterface.bulkInsert("Products",
            [
                {
                    // 1
                    name: "Iphone",
                    sku: "apl_phn",
                    description: "Apple phones",
                    stock: 50,
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 2
                    name: "Iphone 11 Pro",
                    sku: "apl_phn_11p",
                    description: "Iphone 11 pro",
                    stock: 50,
                    parentId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 3
                    name: "Iphone 12 Pro Max",
                    sku: "apl_phn_12pm",
                    description: "Iphone 12 Pro Max",
                    stock: 50,
                    parentId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 4
                    name: "Apple Watch",
                    sku: "apl_wtc",
                    description: "Apple Watch",
                    stock: 50,
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 5
                    name: "Apple Watch SE",
                    sku: "apl_wtc_se",
                    description: "Apple Watch SE",
                    stock: 50,
                    parentId: 4,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 6
                    name: "Dell Laptop",
                    sku: "dll_lptp",
                    description: "Dell Laptop",
                    stock: 20,
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 7
                    name: "Dell Inspiron",
                    sku: "dll_lptp_15",
                    description: '2021 Dell Inspiron 15 3000 Business Laptop, 15.6" Full HD Touchscreen, Intel Core i5-1035G1, 16GB DDR4 RAM, 512GB PCIE SSD, Online Meeting Ready, Webcam, Wi-Fi, HDMI, Windows 10, Black',
                    stock: 20,
                    parentId: 6,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 8
                    name: "Dell Inspiron x",
                    sku: "dll_lptp_13",
                    description: 'Dell Inspiron 13 5310, 13.3 inch QHD (Quad High Definition) Laptop - Thin and Light Intel Core i7-11370H, 16GB DDR4 RAM, 512GB SSD, NVIDIA GeForce MX450, Dell Services - Windows 10 Home ',
                    stock: 20,
                    parentId: 6,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 9
                    name: "Macbook Laptop",
                    sku: "mcbk_lptp",
                    description: "Apple Macbook Laptop",
                    stock: 20,
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 10
                    name: "Macbook Pro Laptop",
                    sku: "mcbk_lptp_pro",
                    description: 'Apple Macbook Pro MJLQ2LL/A 15-inch Laptop, Intel Core i7 Processor, 16GB RAM, 256GB SSD, Mac OS X',
                    stock: 20,
                    parentId: 9,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 11
                    name: "Macbook Air Laptop",
                    sku: "mcbk_lptp_air_m1",
                    description: '2020 Apple MacBook Air Laptop: Apple M1 Chip, 13” Retina Display, 8GB RAM, 512GB SSD Storage, Backlit Keyboard, FaceTime HD Camera, Touch ID. Works with iPhone/iPad; Space Gray',
                    stock: 20,
                    parentId: 9,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 12
                    name: "Macbook Air Laptop",
                    sku: "mcbk_lptp_air",
                    description: 'Apple MacBook Air MD760LL/A 13.3-Inch Laptop (Intel Core i5 Dual-Core 1.3GHz up to 2.6GHz, 4GB RAM, 128GB SSD, Wi-Fi, Bluetooth 4.0)',
                    stock: 20,
                    parentId: 9,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 13
                    name: "Winsome Coffe Table",
                    sku: "wns_cf_tb",
                    description: 'Winsome Suzanne 3-PC Set Space Saver Kitchen, Coffee Finish',
                    stock: 20,
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 14
                    name: "Winsome Coffe Table",
                    sku: "wns_cf_tb_blc",
                    description: 'Winsome Suzanne 3-PC Set Space Saver Kitchen, Coffee Finish, Black',
                    stock: 5,
                    parentId: 13,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 15
                    name: "Winsome Coffe Table",
                    sku: "wns_cf_tb_wht",
                    description: 'Winsome Suzanne 3-PC Set Space Saver Kitchen, Coffee Finish, White',
                    stock: 8,
                    parentId: 13,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 16
                    name: "Becky Farmhouse Dining Table",
                    sku: "bcky_dng_tbl",
                    description: 'Winsome Suzanne 3-PC Set Space Saver Kitchen, Coffee Finish, White',
                    stock: 5,
                    parentId: null,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 17
                    name: "Becky Farmhouse Dining Table",
                    sku: "bcky_dng_tbl_whtsq",
                    description: 'Zinus Becky Farmhouse Square Wood Dining Table, White',
                    stock: 4,
                    parentId: 16,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    // 18
                    name: "Becky Farmhouse Dining Table",
                    sku: "bcky_dng_tbl_whtrd",
                    description: 'Zinus Becky Farmhouse Round Wood Dining Table, White',
                    stock: 1,
                    parentId: 16,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                }
            ],
            {}
        );

        console.log("ProductCategories")

        await queryInterface.bulkInsert("ProductCategories",
        [
            {
                
                productId: 2,
                categoryId: 5,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 3,
                categoryId: 5,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 5,
                categoryId: 8,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 7,
                categoryId: 6,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 8,
                categoryId: 6,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 10,
                categoryId: 6,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 11,
                categoryId: 6,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 12,
                categoryId: 6,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 14,
                categoryId: 12,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 15,
                categoryId: 12,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 17,
                categoryId: 12,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                productId: 18,
                categoryId: 12,
                createdAt: new Date(),
                updatedAt: new Date(),
            }
        ],
        {}
        );

        await queryInterface.bulkInsert("Prices",
        [
            {
                // 1
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 2,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 2
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 3,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 3
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 5,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 4
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 7,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 5
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 8,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 6
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 10,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 7
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 11,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 8
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 12,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 9
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 14,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 10
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 15,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 11
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 17,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                // 12
                title: "",
                try: 1000,
                usd: 50,
                euro: 40,
                salePrice: null,
                productId: 18,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ],
        {}
        );

        console.log("Last")
    },

    down: async (queryInterface, Sequelize) => {
        // Add commands to revert seed here.Example:
        await queryInterface.dropAllTables();
    },
};
