"use strict";

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        console.log("Libraries");

        await queryInterface.bulkInsert(
            "Libraries",
            [
                {
                    path: "src/assets/img/3310.jpg",
                    type: "jpeg",
                    createdAt: new Date(),
                    updatedAt: new Date(),
                }
            ],
            {}
        );
        await queryInterface.bulkInsert(
            "ProductLibraries",
            [
                {
                    productId: 2,
                    libraryId: 1 ,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    productId: 3,
                    libraryId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    productId: 5,
                    libraryId: 1,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                }
            ],
            {}
        );
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    },
};
