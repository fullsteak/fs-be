// const db = require("../models");
// const Role = db.Roles; //! IMPORTANT - models / Products
const db = require("../config/sequelize.config");
const Role = require('../models/Role');

/**
 * Gets all roles
 * @param {Express.request} req
 * @param {Express.response} res 
 * @returns Returns price if found, else returns 404 not found.
 */
 exports.findAllRoles = async(req, res) => {
    const roles = await Role.findAll();

    if (!roles) {
        return res.status(404).send({
            message: `No role found.`,
        });
    }

    return res.status(200).send(roles);
};

/**
 * Gets role by id
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @returns Returns role if found, otherwise returns 404 not found.
 */
exports.getRole = async(req, res) => {
    console.log("Role controller");
    const { id } = req.params;
    const role = await Role.findOne({ where: { id } });

    if (!role) {
        return res.status(404).send({
            message: `No role found with the id ${id}`,
        });
    }

    return res.status(200).send(role);
};

/**
 * Creates new role
 * The user has to pass the jwtValidation
 * @param {Express.Request} req Request that includes role information that are name, isActive
 * @param {Express.Response} res Includes created role's data
 * @returns Returns 201 if succeeded.If the data is not valid or already exists in the database return 400, bad request.
 */
 exports.createRole = async(req, res) => {
    const { name, isActive } = req.body;
    // Checks if the role name exists.
    if ( !name ) {
        return res.status(400).send({
            message: "You need to fill in all fields.",
        });
    }

    // Checks if the role name exists
    let roleExists = await Role.findOne({
        where: { name }
    });
    if (roleExists) return res.status(400).send({message: `A role named ${name} already exists!`});
    
    // Create role
    try {
        let newRole = await Role.create({
            name,
            isActive
        });
        return res.status(201).send(newRole);
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Updates role
 * The user has to pass jwtValidation
 * @param {Express.Request} req Request that includes role information that are name, isActive
 * @param {Express.Response} res b
 * @returns the response
 */
exports.updateRole = async(req, res) => {
    const { name, isActive } = req.body;
    const { id } = req.params;

    const role = await Role.findOne({where: { id } });

    if (!role) {
        return res.status(400).send({
            message: `No role exists with the id ${id}`,
        });
    }

    try {
        //TODO: refactor
        if (name) {
            role.name = name;
        }
        if (isActive) {
            role.isActive = isActive;
        }
        role.save();
        return res.status(200).send({
            message: `Role ${name} has been updated!`,
        });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Deletes role
 * The user has to pass jwtValidation
 * @param {Express.Request} req Request that includes role id as params
 * @param {Express.Response} res 
 * @returns 
 */
exports.deleteRole = async(req, res) => {
    const { id } = req.params;

    if (!id) return res.status(400).send({ message: `Please provide the ID of the role you are trying to delete.` });
    

    const role = await Role.findOne({ where: { id } });

    if (!role) {
        return res.status(400).send({ message: `No role exists with the id ${id}` });
    }

    try {
        await role.destroy();
        return res.status(204).send({ message: `Product ${id} has been deleted.` });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};