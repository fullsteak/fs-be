// const db = require("../models");
// const Price = db.Prices; //! IMPORTANT - models / Prices
const db = require("../config/sequelize.config");
const Price = require('../models/Price');

/**
 * Gets all prices
 * @param {Express.request} req
 * @param {Express.response} res 
 * @returns Returns price if found, else returns 404 not found.
 */
 exports.findAllPrices = async(req, res) => {
    const prices = await Price.findAll();

    if (!prices) {
        return res.status(404).send({
            message: `No price found.`,
        });
    }

    return res.status(200).send(prices);
};

/**
 * Gets price by id
 * @param {Express.request} req request that includes id
 * @param {Express.response} res 
 * @returns Returns price if found, else returns 404 not found.
 */
exports.getPrice = async(req, res) => {
    const { id } = req.params;  
    const price = await Price.findOne({ where: { id } });

    if (!price) {
        return res.status(404).send({
            message: `No price found with the id ${id}`,
        });
    }

    return res.status(200).send(price);
};

//TODO: user info is included within the token. If the jwtValidation middleware returns an access denied, the user data can be logged.
/**
 * Creates new price
 * The user has to pass the jwtValidation
 * @param {Express.request} req Request that includes price information that are name, parentId
 * @param {Express.response} res Includes created price's data
 * @returns Returns 201 if succeeded.If the data is not valid or already exists in the database return 400, bad request.
 */
 exports.createPrice = async(req, res) => {
    const {  title, usd, euro, salesPrice, productId } = req.body;
    const TRY = req.body.try;

    // Checks if the price name exists. parentId can be null if the price is a parent price.
    if ( !title || !productId ) {
        return res.status(400).send({
            message: "You need to fill in all fields.",
        });
    }

    // Checks if the price name exists
    let priceExists = await Price.findOne({
        where: { title }
    });
    if (priceExists) return res.status(400).send({message: `A price named ${title} already exists!`});
    
    // Create price
    try {
        let newPrice = await Price.create({
            title,
            usd,
            euro,
            try:TRY,
            salesPrice,
            productId
        });
        return res.status(201).send(newPrice);
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Updates price
 * The user has to pass jwtValidation
 * @param {Express.body} req Request that includes price information that are name, parentId <nullable>
 * @param {Express.body} res 
 * @returns 
 */
exports.updatePrice = async(req, res) => {
    const {  title, usd, euro, salesPrice, productId } = req.body;
    const TRY = req.body.try;
    const { id } = req.params;

    const price = await Price.findOne({where: { id } });

    if (!price) {
        return res.status(400).send({
            message: `No price exists with the id ${id}`,
        });
    }

    try {
        //TODO: refactor
        if (title) {
            price.title = title;
        }
        if (usd) {
            price.usd = usd;
        }
        if (euro) {
            price.euro = euro;
        }
        if (TRY) {
            price.try = TRY;
        }
        if (salesPrice) {
            price.try = salesPrice;
        }
        if (productId) {
            price.productId = productId;
        }
        price.save();
        return res.status(200).send({
            message: `Price ${title} has been updated!`
        });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Deletes price
 * The user has to pass jwtValidation
 * @param {Express.body} req Request that includes price id as params
 * @param {Express.body} res 
 * @returns 
 */
exports.deletePrice = async(req, res) => {
    const { id } = req.params;

    if (!id) return res.status(400).send({ message: `Please provide the ID of the price you are trying to delete.` });
    

    const price = await Price.findOne({ where: { id } });

    if (!price) {
        return res.status(400).send({ message: `No price exists with the id ${id}` });
    }

    try {
        await price.destroy();
        return res.status(204).send({ message: `Price ${id} has been deleted.` });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};