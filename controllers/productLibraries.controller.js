// const db = require("../models");
// const Product = db.Products; //! IMPORTANT - models / Products
const db = require("../config/sequelize.config");
const ProductLibrary = require('../models/ProductLibrary');


/**
 * Gets all productLibraries
 * @param {Express.request} req
 * @param {Express.response} res 
 * @returns Returns productLibraries if found, else returns 404 not found.
 */
 exports.findAllProductLibraries = async(req, res) => {
    const productLibraries = await ProductLibrary.findAll();

    if (!productLibraries) {
        return res.status(404).send({
            message: `No product library found.`,
        });
    }

    return res.status(200).send(productLibraries);
};

/**
 * Gets product library by id
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @returns Returns product library if found, otherwise returns 404 not found.
 */
exports.getProductLibrary = async(req, res) => {
    console.log("ProductLibrary controller");
    const { id } = req.params;
    const productLibrary = await ProductLibrary.findOne({ where: { id } });

    if (!productLibrary) {
        return res.status(404).send({
            message: `No productLibrary found with the id ${id}`,
        });
    }

    return res.status(200).send(productLibrary);
};

/**
 * Creates new productLibrary
 * The user has to pass the jwtValidation
 * @param {Express.Request} req Request that includes productLibrary information that are productId, libraryId
 * @param {Express.Response} res Includes created productLibrary's data
 * @returns Returns 201 if succeeded.If the data is not valid or already exists in the database return 400, bad request.
 */
 exports.createProductLibrary = async(req, res) => {
    const { productId, libraryId } = req.body;
    // Checks if the productId and libraryId exist.
    if ( !productId || !libraryId ) {
        return res.status(400).send({
            message: "You need to fill in all fields.",
        });
    }

    // Checks if the productId and productLibrary exist in the database.
    let productId_libraryIdExists = await ProductLibrary.findOne({
        where: { productId, libraryId }
    });
    if (productId_libraryIdExists) return res.status(400).send({message: `A product library entry with the productId ${productId} and libraryId ${libraryId} already exists!`});
    
    // Create productLibrary
    try {
        let newProductLibrary = await ProductLibrary.create({
            productId,
            libraryId
        });
        return res.status(201).send(newProductLibrary);
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Updates productLibrary
 * The user has to pass jwtValidation
 * @param {Express.Request} req Request that includes productLibrary information that are productId, libraryId
 * @param {Express.Response} res b
 * @returns the response
 */
exports.updateProductLibrary = async(req, res) => {
    const { productId, libraryId } = req.body;
    const { id } = req.params;

    const productLibrary = await ProductLibrary.findOne({where: { id } });

    if (!productLibrary) {
        return res.status(400).send({
            message: `No productLibrary exists with the id ${id}`,
        });
    }

    try {
        //TODO: refactor
        if (productId) {
            productLibrary.productId = productId;
        }
        if (libraryId) {
            productLibrary.libraryId = libraryId;
        }
        productLibrary.save();
        return res.status(200).send({
            message: `ProductLibrary with id ${id} has been updated!`,
        });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Deletes productLibrary
 * The user has to pass jwtValidation
 * @param {Express.Request} req Request that includes productLibrary id as params
 * @param {Express.Response} res 
 * @returns 
 */
exports.deleteProductLibrary = async(req, res) => {
    const { id } = req.params;

    if (!id) return res.status(400).send({ message: `Please provide the ID of the productLibrary you are trying to delete.` });
    

    const productLibrary = await ProductLibrary.findOne({ where: { id } });

    if (!productLibrary) {
        return res.status(400).send({ message: `No productLibrary exists with the id ${id}` });
    }

    try {
        await productLibrary.destroy();
        return res.status(204).send({ message: `ProductLibrary ${id} has been deleted.` });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};