// const db = require("../models");
// const Product = db.Products; //! IMPORTANT - models / Products
const { Op } = require("sequelize");
const db = require("../config/sequelize.config");
const Product = require('../models/Product');
const Price = require('../models/Price');
const ProductLibrary = require('../models/ProductLibrary');
const ProductCategory = require('../models/ProductCategory');
const Library = require('../models/Library');

/**
 * Gets all products within the given category
 * @param {Express.request} req
 * @param {Express.response} res 
 * @returns Returns products if found, else returns 404 not found.
 */
 exports.findAllProductsByCategory = async(req, res) => {
    const { categoryId } = req.params;
    // let { limit, page } = req.query;
    // page < 1 ? page=1: page ;
    // const offset =  (page * limit - limit);
    const products = await Product.findAll({
        include:
        [
            {model: Price}, // includes Price model
            {
                model: ProductLibrary,  // includes ProductLibrary 
                include: [{model: Library}]
            },
            {
                model: ProductCategory,
                where: {categoryId: categoryId}
            }
        ],
        // limit: limit,
        // offset: offset,
        where: {parentId : {[Op.ne]: null}},
    });

    if (!products) {
        return res.status(404).send({
            message: `No product found.`,
        });
    }

    return res.status(200).send(products);
};

/**
 * Gets all products
 * @param {Express.request} req
 * @param {Express.response} res 
 * @returns Returns products if found, else returns 404 not found.
 */
 exports.findAllProducts = async(req, res) => {
    const products = await Product.findAll();

    if (!products) {
        return res.status(404).send({
            message: `No product found.`,
        });
    }

    return res.status(200).send(products);
};

/**
 * Gets products as paginated. 
 * limit and page query parameters should be given.
 * @param {Express.request} req
 * @param {Express.response} res
 * @returns Returns products and number of rows (count) if found, else returns 404 not found.
 * E.g { count: integer, rows: [ object Array] }
 */
 exports.findAllProductsPaginate = async (req, res) => {

    let { limit, page } = req.query;

    // page = (offset/limit + 1);
    page < 1 ? page=1: page ;

    //const offset = limit * (page - 1);
    const offset =  (page * limit - limit);

    const products = await Product.findAndCountAll({
        include:
        [
            {model: Price}, // includes Price model
            {
                model: ProductLibrary,  // includes ProductLibrary 
                include: [{model: Library}]
            }
        ],
        limit: limit,
        offset: offset,
        where: {parentId : {[Op.ne]: null}},
    });

    if (!products) {
        return res.status(404).send({
            message: `No product found.`,
        });
    }

    return res.status(200).send(products);
};

/**
 * Gets product by id
 * @param {Express.request} req request that includes id
 * @param {Express.response} res 
 * @returns Returns product if found, else returns 404 not found.
 */
exports.getProduct = async(req, res) => {
    const { id } = req.params;
    const product = await Product.findOne({ 
        include:[
            {model: Price},
            {
                model: ProductLibrary, 
                include:[{model:Library}]
            }
        ],
        where: { id } 
    });
    if (!product) {
        return res.status(404).send({
            message: `No product found with the id ${id}`,
        });
    }

    return res.status(200).send(product);
};

/**
 * Creates new product
 * The user has to pass the jwtValidation
 * @param {Express.request} req Request that includes product information that are name, parentIdIncludes created product's data
 * @param {Express.response} res 
 * @returns Returns 201 if succeeded.If the data is not valid or already exists in the database return 400, bad request.
 */
 exports.createProduct = async(req, res) => {
    const { name, sku, description, parentId, stock } = req.body;
    // Checks if the product name exists. parentId can be null if the product is a parent product.
    if ( !name || !sku || !description || !stock ) {
        return res.status(400).send({
            message: "You need to fill in the product name.",
        });
    }

    // Checks if the product name exists
    let productExists = await Product.findOne({
        where: { name }
    });
    if (productExists) return res.status(400).send({message: `A product named ${name} already exists!`});
    
    // Create product
    try {
        
        let newProduct = await Product.create({
            name,
            sku, 
            description,
            parentId,
            stock
        });
        console.log(newProduct);
        return res.status(201).send(newProduct);
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Updates product
 * The user has to pass jwtValidation
 * @param {Express.request} req Request that includes product information that are name, parentId <nullable>
 * @param {Express.response} res 
 * @returns 
 */
exports.updateProduct = async(req, res) => {
    const { name, sku, description, stock, parentId } = req.body;
    const { id } = req.params;

    const product = await Product.findOne({where: { id } });

    if (!product) {
        return res.status(400).send({
            message: `No product exists with the id ${id}`,
        });
    }

    try {
        //TODO: refactor
        if (name) {
            product.name = name;
        }
        if (sku) {
            product.parentId = parentId;
        }
        if (description) {
            product.parentId = parentId;
        }
        if (stock) {
            product.parentId = parentId;
        }
        if (parentId) {
            product.parentId = parentId;
        }
        product.save();
        return res.status(200).send({
            message: `Product ${name} has been updated!`,
        });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Deletes product
 * The user has to pass jwtValidation
 * @param {Express.request} req Request that includes product id as params
 * @param {Express.response} res 
 * @returns 
 */
exports.deleteProduct = async(req, res) => {
    const { id } = req.params;

    if (!id) return res.status(400).send({ message: `Please provide the ID of the product you are trying to delete.` });
    

    const product = await Product.findOne({ where: { id } });

    if (!product) {
        return res.status(400).send({ message: `No product exists with the id ${id}` });
    }

    try {
        await product.destroy();
        return res.status(204).send({ message: `Product ${id} has been deleted.` });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};