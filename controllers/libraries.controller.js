// const db = require("../models");
// const Category = db.Categories; //! IMPORTANT - models / Categories
const db = require("../config/sequelize.config");
const Library = require('../models/Library');

/**
 * Gets all libraries
 * @param {Express.request} req
 * @param {Express.response} res 
 * @returns Returns libraries if found, else returns 404 not found.
 */
 exports.findAllLibraries = async(req, res) => {
    const libraries = await Library.findAll();

    if (!libraries) {
        return res.status(404).send({
            message: `No library found.`,
        });
    }

    return res.status(200).send(libraries);
};

/**
 * Gets library by id
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @returns Returns library if found, otherwise returns 404 not found.
 */
exports.getLibrary = async(req, res) => {
    console.log("Library controller");
    const { id } = req.params;
    const library = await Library.findOne({ where: { id } });

    if (!library) {
        return res.status(404).send({
            message: `No library found with the id ${id}`,
        });
    }

    return res.status(200).send(library);
};

/**
 * Creates new library
 * The user has to pass the jwtValidation
 * @param {Express.Request} req Request that includes library information that are path, type
 * @param {Express.Response} res Includes created library's data
 * @returns Returns 201 if succeeded.If the data is not valid or already exists in the database return 400, bad request.
 */
 exports.createLibrary = async(req, res) => {
    const { path, type } = req.body;
    // Checks if the library path and type exist.
    if ( !path || !type ) {
        return res.status(400).send({
            message: "You need to fill in all fields.",
        });
    }

    // Checks if the library path exists
    let libraryPathExists = await Library.findOne({
        where: { path }
    });
    if (libraryPathExists) return res.status(400).send({message: `A library with the path ${path} already exists!`});
    
    // Create library
    try {
        let newLibrary = await Library.create({
            path,
            type
        });
        return res.status(201).send(newLibrary);
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Updates library
 * The user has to pass jwtValidation
 * @param {Express.Request} req Request that includes library information that are path, type
 * @param {Express.Response} res b
 * @returns the response
 */
exports.updateLibrary = async(req, res) => {
    const { path, type } = req.body;
    const { id } = req.params;

    const library = await Library.findOne({where: { id } });

    if (!library) {
        return res.status(400).send({
            message: `No library exists with the id ${id}`,
        });
    }

    try {
        //TODO: refactor
        if (path) {
            library.path = path;
        }
        if (type) {
            library.type = type;
        }
        library.save();
        return res.status(200).send({
            message: `Library with id ${id} has been updated!`,
        });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Deletes library
 * The user has to pass jwtValidation
 * @param {Express.Request} req Request that includes library id as params
 * @param {Express.Response} res 
 * @returns 
 */
exports.deleteLibrary = async(req, res) => {
    const { id } = req.params;

    if (!id) return res.status(400).send({ message: `Please provide the ID of the library you are trying to delete.` });
    

    const library = await Library.findOne({ where: { id } });

    if (!library) {
        return res.status(400).send({ message: `No library exists with the id ${id}` });
    }

    try {
        await library.destroy();
        return res.status(204).send({ message: `Library ${id} has been deleted.` });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};