// const db = require("../models");
// const UserRole = db.UserRoles; //! IMPORTANT - models / Products

const db = require("../config/sequelize.config");
const UserRole = require('../models/UserRole');


/**
 * Gets all user roles
 * @param {Express.request} req
 * @param {Express.response} res 
 * @returns Returns price if found, else returns 404 not found.
 */
 exports.findAllUserRoles = async(req, res) => {
    const userRoles = await UserRole.findAll();

    if (!userRoles) {
        return res.status(404).send({
            message: `No user role found.`,
        });
    }

    return res.status(200).send(userRoles);
};

/**
 * Gets user role by id
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @returns {string} Returns user role if found, otherwise returns 404 not found.
 */
exports.getUserRole = async(req, res) => {
    console.log("UserRole controller");
    const { id } = req.params;
    const userRole = await UserRole.findOne({ where: { id } });

    if (!userRole) {
        return res.status(404).send({
            message: `No user role found with the id ${id}`,
        });
    }

    return res.status(200).send(userRole);
};

/**
 * Creates new user role
 * The user has to pass the jwtValidation
 * @param {Express.Request} req Request that includes user role information that are userId, roleId
 * @param {Express.Response} res Includes created user role's data
 * @returns Returns 201 if succeeded.If the data is not valid or already exists in the database returns 400, bad request.
 */
 exports.createUserRole = async(req, res) => {
    const { userId, roleId } = req.body;
    // Checks if the userId and roleId are non-null.
    if ( !userId || !roleId) {
        return res.status(400).send({
            message: "You need to fill in all fields.",
        });
    }
    // Check whether there is an entry in the database matching the userId and roleId given in the data.
    let userId_roleIdExist = await UserRole.findOne({
        where: { userId, roleId }
    });
    if (userId_roleIdExist) return res.status(400).send({message: `An entry with userId: ${userId} and roleId: ${roleId} already exists!`});
    
    // Create userRole
    try {
        let newUserRole = await UserRole.create({
            userId,
            roleId
        });
        return res.status(201).send(newUserRole);
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Updates user role
 * The user has to pass jwtValidation
 * @param {Express.Request} req Request that includes user role information that are userId, roleId
 * @param {Express.Response} res 
 * @returns the response
 */
exports.updateUserRole = async(req, res) => {
    const { userId, roleId } = req.body;
    const { id } = req.params;

    const userRole = await UserRole.findOne({where: { id } });

    if (!userRole) {
        return res.status(400).send({
            message: `No user role entry exists with the id ${id}`,
        });
    }

    try {
        //TODO: refactor
        if (userId) {
            userRole.userId = userId;
        }
        if (roleId) {
            userRole.roleId = roleId;
        }
        userRole.save();
        return res.status(200).send({
            message: `User role with id ${id} has been updated!`,
        });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};

/**
 * Deletes user role
 * The user has to pass jwtValidation
 * @param {Express.Request} req Request that includes userId and roleId as params
 * @param {Express.Response} res 
 * @returns 
 */
exports.deleteUserRole = async(req, res) => {
    const { id } = req.params;

    if (!id) return res.status(400).send({ message: `Please provide the ID of the user role entry you are trying to delete.` });
    

    const userRole = await UserRole.findOne({ where: { id } });

    if (!userRole) {
        return res.status(400).send({ message: `No user role exists with the id ${id}` });
    }

    try {
        await userRole.destroy();
        return res.status(204).send({ message: `User role entry with the id: ${id} has been deleted.` });
    } catch (err) {
        return res.status(500).send({
            message: `Error : ${err.message}`,
        });
    }
};